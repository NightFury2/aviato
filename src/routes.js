import React from 'react';
import {IndexRoute, Route} from 'react-router';
import { isLoaded as isAuthLoaded, load as loadAuth } from './redux/modules/Auth/Auth';
import {
    App,
    Login,
    LoginSuccess,
    Prosmotr,
    Registr,
    Home,
    NotFound,
  } from 'containers';

export default (store) => {
  const requireLogin = (nextState, replace, cb) => {
    function checkAuth() {
      const {auth: { user }} = store.getState();
      if (!user) {
        replace('/');
      }
      cb();
    }
    if (!isAuthLoaded(store.getState())) {
      store.dispatch(loadAuth()).then(checkAuth);
    } else {
      checkAuth();
    }
  };
  /**
   * Please keep routes in alphabetical order
   */
  return (
    <Route path="/" component={App}>
      { /* Home (main) route */ }
      <IndexRoute component={Home}/>
      { /* Routes requiring login */ }
      <Route onEnter={requireLogin}>
        <Route path="loginSuccess" component={LoginSuccess}/>
      </Route>
      { /* Catch all route */ }
      <Route path="login" component={Login}/>
      <Route path="registr" component={Registr}/>
      <Route path="prosmotr" component={Prosmotr}/>
      <Route path="*" component={NotFound} status={404} />
    </Route>
  );
};
