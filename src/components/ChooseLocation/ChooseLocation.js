import React, { Component} from 'react';
import Timer from '../Timer/Timer';
import Place from '../Place/Place';

export default class ChooseLocation extends Component {
  static propTypes = {
    setCount: React.PropTypes.func
  };
  state = {
    timer: 0,
    begin: true,
    end: false,
    error: false,
    count: 0,
    errorMassage: ''
  };
  setError = (error, errorMassage) => {
    this.setState({ error: error, errorMassage: errorMassage});
  };
  plusCount = () => {
    this.setState({count: this.state.count + 1});
    this.props.setCount(this.state.count + 1);
  };
  minusCount = () => {
    this.setState({count: this.state.count - 1});
    this.props.setCount(this.state.count - 1);
  };
  render() {
    const { error, errorMassage, count } = this.state;
    const styles = require('./ChooseLocation.scss');
    const Time = error ? (
      <h1><p className="bg-danger">{errorMassage}</p></h1>
    ) : (
      <div className={styles.Timer}>
        <Timer startTime={10} endTimer={this.setError}/>
      </div>
    );
    return (
      <div className={'container-fluid ' + styles.chooseLocation}>
        {Time}
        <div className="row">
          {!error &&
            <div className={'col-xs-12 ' + styles.locationContainer}>
              <div className="row">
                <div className={'col-xs-3 ' + styles.locationContainerBox}>
                  <Place place={1} plusCount={this.plusCount} minusCount={this.minusCount}/>
                  <Place place={3} plusCount={this.plusCount} minusCount={this.minusCount}/>
                  <Place place={5} plusCount={this.plusCount} minusCount={this.minusCount}/>
                  <Place place={7} plusCount={this.plusCount} minusCount={this.minusCount}/>
                  <Place place={2} plusCount={this.plusCount} minusCount={this.minusCount}/>
                  <Place place={4} plusCount={this.plusCount} minusCount={this.minusCount}/>
                  <Place place={6} plusCount={this.plusCount} minusCount={this.minusCount}/>
                  <Place place={8} plusCount={this.plusCount} minusCount={this.minusCount}/>
                </div>
                <div className={styles.infoPlace}>
                  <span>Выбранно {count} мест</span>
                </div>
              </div>
            </div>
          }
        </div>
      </div>
    );
  }
}
