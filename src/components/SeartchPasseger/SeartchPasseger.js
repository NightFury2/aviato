import React, { Component} from 'react';
import FormGroup from 'react-bootstrap/lib/FormGroup';

export default class SeartchPasseger extends Component {
  static propTypes = {
    disable: React.PropTypes.bool,
    setDisable: React.PropTypes.func.isRequired,
    onTabClick: React.PropTypes.func.isRequired,
    onSetAttribute: React.PropTypes.func.isRequired
  };
  state = {
    valueName: '',
    valuePNR: '',
    valueDate: '',
    valueFlightNumber: '',
    errorName: false,
    errorPNR: false,
    errorDate: false,
    errorFlightNumber: false
  };
  handleChangeName = (event) => {
    const input = event.target.value;
    this.setState({valueName: input});
    if (input !== '') {
      this.setState({errorName: false});
    } else {
      this.setState({errorName: true});
    }
  };
  handleChangePNR = (event) => {
    const input = event.target.value;
    this.setState({valuePNR: input});
    if (input !== '') {
      this.setState({errorPNR: false});
    } else {
      this.setState({errorPNR: true});
    }
  };
  handleChangeDate = (event) => {
    const input = event.target.value;
    this.setState({valueDate: input});
    if (input !== '') {
      this.setState({errorDate: false});
    } else {
      this.setState({errorDate: true});
    }
  };
  handleChangeFlightNumber = (event) => {
    const input = event.target.value;
    this.setState({valueFlightNumber: input});
    if (input !== '') {
      this.setState({errorFlightNumber: false});
    } else {
      this.setState({errorFlightNumber: true});
    }
  };
  handleClick = () => {
    if (this.state.valueName !== '') {
      this.setState({errorName: false});
    } else {
      this.setState({errorName: true});
    }
    if (this.state.valuePNR !== '') {
      this.setState({errorPNR: false});
    } else {
      this.setState({errorPNR: true});
    }
    if (this.state.valueDate !== '') {
      this.setState({errorDate: false});
    } else {
      this.setState({errorDate: true});
    }
    if (this.state.valueFlightNumber !== '') {
      this.setState({errorFlightNumber: false});
    } else {
      this.setState({errorFlightNumber: true});
    }
    if (this.state.valueName !== ''
      && this.state.valuePNR !== ''
      && this.state.valueDate !== ''
      && this.state.valueFlightNumber !== '') {
      this.props.setDisable(false);
      this.props.onTabClick(2);
      this.props.onSetAttribute(
        this.state.valueName,
        this.state.valuePNR,
        this.state.valueDate,
        this.state.valueFlightNumber
      );
    } else {
      this.props.setDisable(true);
    }
  };
  render() {
    const {errorName, errorPNR, errorDate, errorFlightNumber} = this.state;
    const styles = require('./SeartchPasseger.scss');
    const validationStateName = errorName ? 'error' : 'success';
    const validationStatePNR = errorPNR ? 'error' : 'success';
    const validationStateDate = errorDate ? 'error' : 'success';
    const validationStateFlightNumber = errorFlightNumber ? 'error' : 'success';
    return (
      <div className={styles.masthead}>
        <div className="container-fluid">
          <div className="row">
            <form >
              <div className="col-xs-3">
                <FormGroup
                  bsStyle={validationStateName}
                  controlId="formName"
                >
                  <input
                    className={'form-control disable'}
                    type="text"
                    value={this.state.valueName}
                    placeholder="Фамилия пассажира"
                    onChange={this.handleChangeName}
                  />
                  {errorName &&
                    <span className="help-block">Введите имя</span>
                  }
                </FormGroup>
              </div>
              <div className="col-xs-3">
                <FormGroup
                  bsStyle={validationStatePNR}
                  controlId="formPNR"
                >
                  <input
                    className="form-control"
                    type="text"
                    value={this.state.valuePNR}
                    placeholder="Номер PNR/Номер билета"
                    onChange={this.handleChangePNR}
                  />
                  {errorPNR &&
                    <span className="help-block">Введите Номер PNR/Номер билета</span>
                  }
                </FormGroup>
              </div>
              <div className="col-xs-3">
                <FormGroup
                  bsStyle={validationStateDate}
                  controlId="formDate"
                >
                  <input
                    className="form-control"
                    type="date"
                    value={this.state.valueDate}
                    placeholder="Дата"
                    onChange={this.handleChangeDate}
                  />
                  {errorDate &&
                    <span className="help-block">Введите дату</span>
                  }
                </FormGroup>
              </div>
              <div className="col-xs-3">
                <FormGroup
                  bsStyle={validationStateFlightNumber}
                  controlId="formFlightNumber"
                >
                  <input
                    className="form-control"
                    type="text"
                    value={this.state.valueFlightNumber}
                    placeholder="Номер рейса"
                    onChange={this.handleChangeFlightNumber}
                  />
                  {errorFlightNumber &&
                    <span className="help-block">Введите номер рейса</span>
                  }
                </FormGroup>
              </div>
              <div className={styles.formBtn}>
                <button type="button" className="btn btn-primary" onClick={this.handleClick}>{' '}Поиск</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
