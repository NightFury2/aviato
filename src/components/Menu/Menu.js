import React, { Component } from 'react';
import {connect} from 'react-redux';
import Nav from 'react-bootstrap/lib/Nav';
import NavItem from 'react-bootstrap/lib/NavItem';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';
import Tooltip from 'react-bootstrap/lib/Tooltip';
import {LinkContainer} from 'react-router-bootstrap';
import {isOpen} from '../../redux/modules/Menu/Menu';
import {HOME, LOGIN, LOGIN_SUCCESS, PROSMOTR, REGISTR} from './ConstMenu';

@connect(
  state => ({ open: state.Menu.open, user: state.auth.user }),
  {isOpen}
)

export default class Menu extends Component {
  static propTypes = {
    open: React.PropTypes.bool,
    user: React.PropTypes.object,
    isOpen: React.PropTypes.func.isRequired
  };
  handleClick = () => {
    this.props.isOpen(this.props.open);
  };
  render() {
    const {open, user} = this.props;
    const stules = require('./Menu.scss');
    const homelabel = open ? HOME : '';
    const prosmotrLabel = open ? PROSMOTR : '';
    const registrLabel = open ? REGISTR : '';
    const loginLabel = open ? LOGIN : '';
    const loginSuccess = open ? LOGIN_SUCCESS : '';
    const titleMenu = open ? 'скрыть меню' : 'открыть меню';
    const tooltip = (
      <Tooltip id="tooltip"><strong>{titleMenu}</strong></Tooltip>
    );
    const menuHidden = open ? 'fa fa-angle-double-left' : 'fa fa-angle-double-right';
    // require the logo image both from client and server
    return (
      <div className={stules.masthead + ' row'}>
        <div className={stules.menuContainer}>
          <h1>Меню</h1>
          <Nav stacked activeKey={1}>
            <LinkContainer to="/">
              <NavItem eventKey={1}>
                <p><i className="fa fa-home" aria-hidden="true"> {homelabel}</i></p>
              </NavItem>
            </LinkContainer>
            <LinkContainer to="/prosmotr">
              <NavItem eventKey={2}>
                <p><i className="fa fa-address-card-o" aria-hidden="true"> {prosmotrLabel}</i></p>
              </NavItem>
            </LinkContainer>
            <LinkContainer to="/registr">
              <NavItem eventKey={3}>
                <p><i className="fa fa-plane" aria-hidden="true"> {registrLabel}</i></p>
              </NavItem>
            </LinkContainer>
            {user &&
              <LinkContainer to="/loginSuccess">
                <NavItem eventKey={4}>
                  <p><i className="fa fa-user" aria-hidden="true"> {loginSuccess}</i></p>
                </NavItem>
              </LinkContainer>
            }
            {!user &&
              <LinkContainer to="/login">
                <NavItem eventKey={4}>
                  <p><i className="fa fa-user" aria-hidden="true"> {loginLabel}</i></p>
                </NavItem>
              </LinkContainer>
            }
            <OverlayTrigger placment="right" overlay={tooltip}>
              <NavItem eventKey={5} onClick={this.handleClick}>
                <p><i className={menuHidden} aria-hidden="true"></i></p>
              </NavItem>
            </OverlayTrigger>
          </Nav>
        </div>
      </div>
    );
  }
}
