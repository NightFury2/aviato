import React, { Component } from 'react';

export default class BoardingPass extends Component {
  static propTypes = {
    count: React.PropTypes.number.isRequired,
    name: React.PropTypes.string.isRequired,
    numberPNR: React.PropTypes.string.isRequired,
    date: React.PropTypes.string.isRequired,
    flightNumber: React.PropTypes.string.isRequired
  };
  render() {
    const {count, name, numberPNR, date, flightNumber} = this.props;
    const styles = require('./BoardingPass.scss');
    // require the logo image both from client and server
    return (
      <div className={styles.masthead}>
        <div className="container-fluid">
          <div className="row">
            <h2>Посадочный таллон </h2>
            <div className={'pull-left ' + styles.info}>
              <span>Имя: <span className="label label-info">{name}</span></span>
              <span>Номер билета: <span className="label label-info">{numberPNR}</span></span>
              <span>Дата вылета: <span className="label label-info">{date}</span></span>
              <span>Номер рейса: <span className="label label-info">{flightNumber}</span></span>
              <span>Количество мест: <span className="label label-info">{count}</span></span>
            </div>
          </div>
        </div>
        <div className="alert alert-success">Вы успешно зарегистрировались на рейс</div>
      </div>
    );
  }
}
