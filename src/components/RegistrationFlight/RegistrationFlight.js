import React, { Component } from 'react';
import ChooseLocation from '../ChooseLocation/ChooseLocation';

export default class RegistrationFlight extends Component {
  static propTypes = {
    user: React.PropTypes.object,
    setCount: React.PropTypes.func.isRequired,
    onTabClick: React.PropTypes.func.isRequired,
    name: React.PropTypes.string.isRequired,
    numberPNR: React.PropTypes.string.isRequired,
    date: React.PropTypes.string.isRequired,
    flightNumber: React.PropTypes.string.isRequired
  };
  state = {
    count: 0,
    openRegistration: false,
    openChooseLocation: false
  };
  setChooseLocation = () => {
    this.setState({openChooseLocation: true});
  };
  setCount = (count) => {
    this.setState({count: count});
    this.props.setCount(count);
  };
  handleClick = () => {
    this.props.onTabClick(3);
  };
  render() {
    const {name, numberPNR, date, flightNumber, user} = this.props;
    const {openChooseLocation} = this.state;
    const styles = require('./RegistrationFlight.scss');
    return (
      <div className={styles.masthead}>
        <div className="container-fluid">
          <div className="row">
            <h2>Регистрация на рейс</h2>
            <div className="container-fluid">
              <div className={'row ' + styles.registrationTab}>
                <div className={'col-xs-2 ' + styles.photoTab + ' img-circle'}>
                </div>
                <div className={'col-xs-2 ' + styles.nameTab}>
                  <h4><span className="label label-default">Имя Пассажира</span></h4>
                  <span>{name}</span>
                </div>
                <div className={'col-xs-2 ' + styles.numberPNRTab}>
                  <h4><span className="label label-default">Номер билета</span></h4>
                  <span>{numberPNR}</span>
                </div>
                <div className={'col-xs-3 ' + styles.dateTab}>
                  <h4><span className="label label-default">Дата регистрации</span></h4>
                  <span>{date}</span>
                </div>
                <div className={'col-xs-2 ' + styles.flightNumberTab}>
                  <h4><span className="label label-default">Номер рейса</span></h4>
                  <span>{flightNumber}</span>
                </div>
                {user &&
                  <div className={'col-xs-2 '}>
                    <button className="btn btn-primary" onClick={this.setChooseLocation}>Выбрать место</button>
                  </div>
                }
              </div>
            </div>
            {!user &&
              <h3 className={'bg-danger ' + styles.informMassage}>Прежде чем продолжить пожалуйста авторизируйтесь или заригистрируйтесь</h3>
            }
            {openChooseLocation &&
              <ChooseLocation setCount={this.setCount}/>
            }
            <br/>
            {user &&
              <button type="button" className="btn btn-primary" onClick={this.handleClick}>{' '}Зарегистрироваться на рейс</button>
            }
          </div>
        </div>
      </div>
    );
  }
}
