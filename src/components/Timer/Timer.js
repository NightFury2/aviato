import React from 'react';

export default class Timer extends React.Component {
  static propTypes = {
    startTime: React.PropTypes.number.isRequired,
    endTimer: React.PropTypes.func
  };
  state = {
    min: this.props.startTime,
    seconds: 0
  };
  componentDidMount = () => {setInterval(this.tick, 1000);};
  tick = () => {
    if (this.state.min === 0 && this.state.seconds === 0) {
      clearInterval(this.props.endTimer(true, 'Время вышло!'));
    } else if (this.state.seconds === 0) {
      this.setState({ min: this.state.min - 1, seconds: 60 });
    } else {
      this.setState({ seconds: this.state.seconds - 1 });
    }
  };
  render() {
    const {min, seconds} = this.state;
    const styles = require('./Time.scss');
    return (
      <div className={'col-xs-1 ' + styles.timeContainer}>
        <div className={styles.timeBox}>{min + ' : ' + seconds}</div>
      </div>
    );
  }
}
