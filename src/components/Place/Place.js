import React from 'react';

export default class Place extends React.Component {
  static propTypes = {
    place: React.PropTypes.number,
    plusCount: React.PropTypes.func,
    minusCount: React.PropTypes.func
  };
  state = {
    active: false,
    isBusy: false
  };
  setActive = () => {
    if (!this.state.active) {
      this.props.plusCount();
    } else {
      this.props.minusCount();
    }
    this.setState({active: !this.state.active});
  };
  render() {
    const {place} = this.props;
    const {active} = this.state;
    const styles = require('./Place.scss');
    const activeDiv = active ? styles.palceActive : ' ';
    return (
      <div className={styles.placeContainer + ' ' + activeDiv} onClick={this.setActive}>
        <span className={styles.placeLabel}>{place}</span>
      </div>
    );
  }
}
