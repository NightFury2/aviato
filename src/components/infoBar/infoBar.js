import React, { Component, PropTypes } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {load} from '../../redux/modules/Info/Info';

@connect(
  state => ({info: state.info.data, loading: state.info.loading}),
  dispatch => bindActionCreators({load}, dispatch)
)

export default class infoBar extends Component {
  static propTypes = {
    info: PropTypes.object,
    loading: PropTypes.bool,
    load: PropTypes.func.isRequired
  };
  loadServer = (event) => {
    event.preventDefault();
    this.props.load();
  };
  render() {
    const {info, loading} = this.props;
    const loadingIcon = loading ? 'fa fa-spinner fa-spin' : 'fa fa-spinner';
    const styles = require('./infoBar.scss');
    return (
      <div className={' well'}>
        <div className="container-fluid">
          <div className="row">
            <strong>{info ? info.message : 'no info'}</strong>
            <span className={styles.time}>{info && new Date(info.time).toString()}</span>
            <button className="btn btn-primary" onClick={this.loadServer}><i className={loadingIcon}></i> Перезапустить сервер</button>
          </div>
        </div>
      </div>
    );
  }
}
