import React, { Component } from 'react';

export default class Home extends Component {
  render() {
    const styles = require('./Home.scss');
    return (
      <div className={styles.masthead}>
        <h1>Главная</h1>
        <div className="container-fluid">
          <div className="row">
            <div className={styles.contentHome}>
              <div className={styles.carusel}>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
