import React, { Component, PropTypes } from 'react';
import Tabs from 'react-bootstrap/lib/Tabs';
import Tab from 'react-bootstrap/lib/Tab';
import { SeartchPasseger, RegistrationFlight, BoardingPass } from '../../components';
import {connect} from 'react-redux';

@connect(state => ({ user: state.auth.user }))
export default class Registr extends Component {
  static propTypes = {
    user: PropTypes.object
  };
  state = {
    count: 0,
    key: 1,
    showSearch: false,
    name: '',
    numberPNR: '',
    date: '',
    flightNumber: '',
    disable: true
  };
  setDisable = (disable) => this.setState({disable});
  setCount = (count) => {
    this.setState({count: count});
  };
  setAttribute = (name, numberPNR, date, flightNumber) => {
    this.setState({ name, numberPNR, date, flightNumber});
  };
  handleClick = (key) => {
    this.setState({key});
  };
  handleSelect = (key) => {
    this.setState({key});
  };
  render() {
    const {key, name, numberPNR, date, flightNumber, disable, count} = this.state;
    const {user} = this.props;
    const styles = require('./Registr.scss');
    const disabledRegistrationFlightTab = disable ? 'disabled' : '';
    const disableBoardingPassTab = user ? '' : 'disabled';
    return (
      <div className={styles.masthead}>
        <h1>Регистрация на рейс</h1>
        <div className="container-fluid">
          <div className="row">
            <Tabs activeKey={key} onSelect={this.handleSelect}>
              <Tab eventKey={1} title="Поиск Пассажира">
                <SeartchPasseger disable={disable} setDisable={this.setDisable} onTabClick={this.handleClick} onSetAttribute={this.setAttribute}/>
              </Tab>
              <Tab eventKey={2} disabled={disabledRegistrationFlightTab} title="Регистрация на рейс">
                <RegistrationFlight setCount={this.setCount} name={name} user={user} numberPNR={numberPNR} date={date} flightNumber={flightNumber} onTabClick={this.handleClick}/>
              </Tab>
              <Tab eventKey={3} disabled={disableBoardingPassTab} title="Посадочный талон">
                <BoardingPass count={count} name={name} user={user} numberPNR={numberPNR} date={date} flightNumber={flightNumber} />
              </Tab>
            </Tabs>
          </div>
        </div>
      </div>
    );
  }
}
