import React, { Component } from 'react';


export default class NotFound extends Component {
  render() {
    const styles = require('./NotFound.scss');
    const errrorImage404 = require('./404.png');
    // require the logo image both from client and server
    return (
      <div className={styles.masthead}>
        <img src={errrorImage404}/>
      </div>
    );
  }
}
