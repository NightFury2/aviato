import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import { logout } from '../../redux/modules/Auth/Auth';

@connect(
  state => ({user: state.auth.user}),
  { logout }
)
export default class LoginSuccess extends Component {
  static propTypes = {
    user: PropTypes.object,
    logout: PropTypes.func
  };
  logoutUser = (event) => {
    event.preventDefault();
    this.props.logout();
  };
  render() {
    const { user } = this.props;
    const styles = require('./LoginSuccess.scss');
    const photoAvatar = require('./dummy_user.png');
    return (
      <div className={styles.loginSuccess}>
        {user &&
          <div className="container-fluid">
            <div className="row">
              <div className={styles.loginSuccessContent}>
                <div className="row">
                  <div className="col-xs-3">
                    <img src={photoAvatar} className="img-circle"/>
                  </div>
                  <div className="col-xs-8">
                    <p>Вы вошли в систему как {user.name}.</p>
                    <button className="btn btn-danger" onClick={this.logoutUser}><i className="fa fa-sign-out">{' '}Выход</i></button>
                  </div>
                </div>
                <div>
                  <p>Информация про юзера</p>
                </div>
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}
