import React, { Component, PropTypes } from 'react';
import {connect} from 'react-redux';
import Helmet from 'react-helmet';
import * as authAction from '../../redux/modules/Auth/Auth';


@connect(
  state => ({user: state.auth.user, inputError: state.auth.inputError, loggingIn: state.auth.loggingIn}),
  authAction
)

export default class Login extends Component {
  static propTypes = {
    user: PropTypes.object,
    inputError: PropTypes.bool,
    loggingIn: PropTypes.bool,
    login: PropTypes.func,
    setInputNotError: PropTypes.func,
    setInputError: PropTypes.func
  };
  handleSubmit = (event) => {
    event.preventDefault();
    const input = this.refs.username;
    this.props.setInputNotError();
    this.props.login(input.value);
    input.value = '';
  };
  render() {
    const {user, inputError, loggingIn} = this.props;
    const styles = require('./Login.scss');
    const loading = loggingIn ? 'fa fa-spinner fa-spin' : 'fa fa-sign-in';
    const inputErrors = inputError ? 'has-error' : '';
    return (
      <div className={styles.login}>
        <Helmet title="Вход"/>
        {!user &&
          <div className={styles.loginContent}>
            <h1>Вход</h1>
            <div>
              <form>
                <div className={'form-group ' + inputErrors}>
                  <input type="text" ref="username" placeholder="Введите ваше имя" className="form-control"/>
                </div>
                <button className="btn btn-success" onClick={this.handleSubmit}>
                  <i className={loading}></i>{' '}Вход
                </button>
              </form>
            </div>
          </div>
        }
      </div>
    );
  }
}
