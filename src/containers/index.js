export App from './App/App';
export Auth from './Auth/Auth';
export Login from './Login/Login';
export Prosmotr from './Prosmotr/Prosmotr';
export Registr from './Registr/Registr';
export Home from './Home/Home';
export NotFound from './NotFound/NotFound';
export LoginSuccess from './LoginSuccess/LoginSuccess';
