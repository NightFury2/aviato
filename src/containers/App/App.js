import React, { Component, PropTypes } from 'react';
import Helmet from 'react-helmet';
import config from '../../config';
import { Menu } from './../../components';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {push} from 'react-router-redux';
import { isLoaded as isAuthLoaded, load as loadAuth, logout } from '../../redux/modules/Auth/Auth';
import { asyncConnect } from 'redux-async-connect';

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];
    if (!isAuthLoaded(getState())) {
      promises.push(dispatch(loadAuth()));
    }
    return Promise.all(promises);
  }
}])

@connect(
  state => ({ open: state.Menu.open, user: state.auth.user }),
  {logout, pushState: push}
)

export default class App extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    open: PropTypes.bool,
    user: PropTypes.object,
    logout: PropTypes.func.isRequired,
    pushState: PropTypes.func.isRequired
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };
  componentWillReceiveProps(nextProps) {
    if (!this.props.user && nextProps.user) {
        // login
      this.props.pushState('/loginSuccess');
    } else if (this.props.user && !nextProps.user) {
        // logout
      this.props.pushState('/');
    }
  }
  handleLogout = (event) => {
    event.preventDefault();
    this.props.logout();
  };
  render() {
    const {open, user} = this.props;
    const styles = require('./App.scss');
    const menuLength = open ? 2 : 1;
    const contantLength = 12 - menuLength;
    return (
      <div className={styles.app}>
        <Helmet {...config.app.head}/>
        <nav className="navbar navbar-fixed-top navbar-default">
            <div className="navbar-header">
              <Link to="/">
                <div className={styles.brand}></div>
              </Link>
            </div>
            <div className="collapse navbar-collapse">
              <ul className="nav navbar-nav navbar-right">
                <li className={styles.login}>
                  {user &&
                    <Link className={styles.loginText} to="/logout" onClick={this.handleLogout}>
                     Выход
                    </Link>}
                  {!user &&
                    <Link className={styles.loginText} to="/login">
                     Вход
                    </Link>}
                </li>
              </ul>
            </div>
        </nav>
        <div className="container-fluid">
          <div className="row">
            <div className={'col-xs-' + menuLength}>
              <div className="row">
                <Menu />
              </div>
            </div>
            <div className={'col-xs-' + contantLength}>
              <div className="row">
                {this.props.children}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
