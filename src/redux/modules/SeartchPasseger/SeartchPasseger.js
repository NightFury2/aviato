import {
  DISABLE_FALSE,
  DISABLE_TRUE,
  SET_KEY
} from './ConstSeartchPasseger';

const initialState = {
  disable: true,
  key: 2
};

export default function SeartchPasseger(state = initialState, action) {
  switch (action.type) {
    case DISABLE_TRUE:
      return {
        ...state,
        disable: true
      };
    case DISABLE_FALSE:
      return {
        ...state,
        disable: false
      };
    case SET_KEY:
      return {
        ...state,
        key: action.key
      };
    default:
      return state;
  }
}
export function setDisableTrue() {
  return {type: DISABLE_TRUE};
}
export function setDisableFalse() {
  return {type: DISABLE_FALSE};
}
export function setKey(key) {
  return {type: SET_KEY, key
  };
}
