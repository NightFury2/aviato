import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import {reducer as reduxAsyncConnect} from 'redux-async-connect';

import Menu from './Menu/Menu';
import auth from './Auth/Auth';
import info from './Info/Info';

export default combineReducers({
  routing: routerReducer,
  reduxAsyncConnect,
  Menu,
  auth,
  info
});
