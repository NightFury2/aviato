const MENU_OPEN = 'MENU_OPEN';
const MENU_CLOSE = 'MENU_CLOSE';

const initialState = {
  open: true
};

export default function menu(state = initialState, action) {
  switch (action.type) {
    case MENU_OPEN:
      return {
        ...state,
        open: true
      };
    case MENU_CLOSE:
      return {
        ...state,
        open: false
      };
    default:
      return state;
  }
}

export function isOpen(open) {
  return {type: open ? MENU_CLOSE : MENU_OPEN};
}
